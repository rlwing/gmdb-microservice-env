# gMDb Microservice Environment

## Cloning this Repository

This repository is made up of many repositories, use the following command to clone this repository in your local environment...

`$ git clone --recursive <this projects url> `

see article [Working with Submodules](https://github.blog/2016-02-01-working-with-submodules/)

## Environment

- MySql database, exposed locally on port 3309 as to not conflict with user's installed database.
- NetFlix Eureka Service Registry exposed locally on port 8761
- NetFlix Zuul Gateway server exposed locally on port 8000 with the following elements
    - Cors configuration
    - Spring Security with a JWT Authentication Filter
        - Routes
            - /auth/** => gmdb-auth-svc (service defined below)
            - /register/** => gmdb-user-svc (developer to provide endpoint)
            - /password/** => gmdb-user-svc (developer to provide endpoint)
            - /api/movies/** => gmdb-movie-svc (developer to provide endpoint)
            - /api/reviews/** => gmdb-review-svc (developer to provide endpoint)
            - /api/users/** => gmdb-user-svc (developer to provide endpoint)
        - Security Config
            - POST - permitAll() => /auth/**, /register/**
            - GET - permitAll() => /api/movies/**, /api/reviews/**
            - PUT - hasRole("ADMIN") => /admin/**
            - POST - hasRole("USER") => /password/** /api/users/**
- Username/Password Authentication service that is not exposed locally.
    - Returns "Authentication" header with a JWT Token 
    - POST - permitAll() => /auth/**
    - PUT - hasRole("ADMIN") => /admin/**

NOTE: You must configure a docker bridge network named gmdb-bridg in order to use this environment. `$ docker network create gmdb-bridge`

## Verifying your environment

- MySql - `$ mysql -h127.0.0.1 -P3309 -ugmdb_app -p1234 gmdb `
- Discovery / Service Registry Server - http://localhost:8761
- Authentication
    - Endpoint: http://localhost:8000/auth/
    - Header: Content-Type application/json
    - body:
    ```
    {
	      "username": "user@gmdb.com",
	      "password": "password"
    }
    ```
    - Request should return a 200, and there should be a response header named 
      "Authorization" with a value that starts with "Bearer " 
    - NOTE: It may take a minute or two for the service registry to recognize the auth service..
    - If you receive a 404, or 500 error, check to see if the service is running with ...
        `$ docker-compose ps` 
        you can start the auth service with...
        `$ docker-compose start auth`

## Setup and run instructions
1. Clone this repository 
`$ git clone --recursive <this projects url> `
1. cd into the project directory
1. cd into the compose directory
1. Create a docker bridge network 
  `$ docker network create gmdb-bridge `
1. Build the docker imagess
  `$ docker-compose build `
1. Bring up the environment
  `$ docker-compose up -d`

## Docker Compose commands you may find useful

- Check to see if your services are running.
`$ docker-compose ps ` 
- Start a single service 
`$ docker-compose start [service name from yml]`
- Stop your containers, but keep available `$ docker-compose stop `
- Stop your containers and remove them `$ docker-compose down ` NOTE: Volumes are retained.
- Stop your containers, remove them and any associated volumes `$ docker-compose down -v `

