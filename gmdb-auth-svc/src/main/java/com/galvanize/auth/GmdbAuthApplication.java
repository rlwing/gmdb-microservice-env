package com.galvanize.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan(basePackages = {"com.galvanize.auth"})
public class GmdbAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(GmdbAuthApplication.class, args);
    }

}
