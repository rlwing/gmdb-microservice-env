#!/bin/sh

docker run -d -p 80:8000 \
         -e EUREKA_CLIENT_ENABLED=true \
         -e EUREKA_HOST=gmdb-discovery:8761 \
         --network gmdb-bridge \
         gmdb/zuell-gateway
